#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QLabel>
#include <QSystemTrayIcon>
#include <QTimer>
#include <QMenu>

class ProcInfoUtils;
namespace Ui {
    class MainWindow;
}

class NotificationUtils;
class MainWindow : public QWidget
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = nullptr);

    QString getCpuUsed();
    QString getMemUsed();
    QString getNetUpload();
    QString getNetDownload();
    QString getUptime();

private:
    Ui::MainWindow *ui;

    QLabel *mCpuUsed;
    QLabel *mMemUsed;
    QLabel *mNetUpload;
    QLabel *mNetDownload;
    QLabel *mUptime;

    QTimer *timer;
    ProcInfoUtils *procInfo;
    NotificationUtils *notify;

//![1 任务栏图标与菜单控制]
    QSystemTrayIcon trayIcon;
    QTimer *showHideTimer;
    bool workspaceShow;
    QMenu *menu;
    QAction *a, *b;
//![1]

//![2 重叠位置计算]
    bool mouse_inner;
    bool top_or_bottom;
    QPoint m_position;
//![2]

private slots:
    void onTimerout();
};

#endif // MAINWINDOW_H
