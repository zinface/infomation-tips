#ifndef __NOTIFICATIONUTILS__H__
#define __NOTIFICATIONUTILS__H__

#define QT_NO_KEYWORDS
#include <QObject>

#define NOTIFY_TIMEOUT -1
#define NOTIFY_ICON_PATH "/usr/share/infomation-tips/infomation.png"

/**
 * @brief  基于 libnotify 的提示器类
 * @note   默认情况下将在
 * @retval None
 */
class NotificationUtils : public QObject {
    Q_OBJECT
public:
    explicit NotificationUtils(QString summary = "", QObject *parent = nullptr);
    ~NotificationUtils();

    static void sendNotification(const QString &message, int timeout = NOTIFY_TIMEOUT, const QString &icon = NOTIFY_ICON_PATH);
    // static void sendNotification(const char *message, int timeout = NOTIFY_TIMEOUT, const QString &icon = NOTIFY_ICON_PATH);

};

#endif  //!__NOTIFICATIONUTILS__H__