#include "NotificationUtils.h"

#include <libnotify/notify.h>

static QString _summary = "";
NotificationUtils::NotificationUtils(QString summary, QObject *parent) : QObject(parent) 
{
    _summary = summary;
    notify_init(_summary.toLocal8Bit());
}

NotificationUtils::~NotificationUtils()
{
    notify_uninit();
}

static NotifyNotification *_notify = nullptr;

void NotificationUtils::sendNotification(const QString &message, int timeout, const QString &icon) {
    if (_notify == nullptr) {
        _notify = notify_notification_new(_summary.toLocal8Bit(), message.toLocal8Bit(), icon.toLocal8Bit());
        notify_notification_set_timeout(_notify, timeout);
    }

    notify_notification_update(_notify, _summary.toLocal8Bit(), message.toLocal8Bit(), icon.toLocal8Bit());

    notify_notification_show(_notify, nullptr);
}

// void NotificationUtils::sendNotification(const char *message, int timeout, const QString &icon) {
//     if (_notify == nullptr) {
//         _notify = notify_notification_new(_summary.toLocal8Bit(), message, icon.toLocal8Bit());
//         notify_notification_set_timeout(_notify, timeout);
//     }
    
//     notify_notification_update(_notify, _summary.toLocal8Bit(), message, icon.toLocal8Bit());
    
//     notify_notification_show(_notify, nullptr);
// }

