#ifndef __PROCINFOUTILS__H__
#define __PROCINFOUTILS__H__

#include <QObject>

class ProcInfoUtils
{
public:
    explicit ProcInfoUtils();
   
    enum SpeedRateUnit {
        SpeedRateBit,
        SpeedRateByte,
        SpeedRateKb,
        SpeedRateMb,
        SpeedRateGb,
        SpeedRateTb,
        SpeedRateUnknow
    };
    enum NetworkMode {
        UP,
        DOWN,
    };
    enum CpuMode {
        ALL,
        FREE
    };
    enum MemMode {
        PHYSICAL,
        VIRTUAL,
    };
    
    QString convertRateUnits(SpeedRateUnit &unit);
    QString autoRateUnits(long speed, SpeedRateUnit &unit, double &sp);

    double getLinuxCpuPercent();
    QList<double> getLinuxMemPercent();
    double getLinuxNetworkUp();
    double getLinuxNetworkDownload();
    double getLinuxUptime();

private:
    double readCpu(CpuMode mode);
    double readCpuPercent();
    double readCpuPercentSnapshot(); /** 100.0 */

    QList<double> readMem(MemMode mode);
    double readMemPercent(MemMode mode);
    double readMemPhysicalPercent();
    double readMemVirtualPercent();

    long readNetwork(NetworkMode mode);
    double readNetworkUp();
    double readNetworkDown();
    double readNetworkSnapshotUp();
    double readNetworkSnapshotDown();

    double readUptime();

    long old_cpuAll = 0;
    long old_cpuFree = 0;
    long old_uploadAll = 0;
    long old_downloadAll = 0;
    
};

#endif  //!__PROCINFOUTILS__H__
