# 提供给其它插件项目引用的共享资源(如果其它插件想通过框架构建引用的话）
if(FRAMEWORK_WANT_INCLUDE)
    
    # procutils
    if(feature_procutils)
        list(APPEND FRAMEWORK_INCLUDE_EXPORTS      # 共享的引用目录 - 用于 include
            ${CMAKE_CURRENT_LIST_DIR}/    
            ${CMAKE_CURRENT_LIST_DIR}/utils/ProcInfoUtils/
        )
        list(APPEND FRAMEWORK_SOURCES_EXPORTS      # 共享的资源文件 - 用于 构建
            ${CMAKE_CURRENT_LIST_DIR}/utils/ProcInfoUtils/*.h
            ${CMAKE_CURRENT_LIST_DIR}/utils/ProcInfoUtils/*.cpp
        )
    endif(feature_procutils)

    return()
endif(FRAMEWORK_WANT_INCLUDE)