#include <QCoreApplication>

#include "NotificationUtils.h"

int main(int argc, char **argv) {
    QCoreApplication app(argc, argv);
    NotificationUtils utils("testNotification");
    utils.sendNotification("太好了，这个测试程序已经成功运行.");
    return 0;
}