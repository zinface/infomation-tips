#include <stdio.h>
#include <string.h>
#include <libnotify/notify.h>

int	main(int argc, char *argv[])
{
    char * notify_body = "内存要炸了!";

    notify_init("infomation-tips");
    NotifyNotification * notify = NULL;
    notify = notify_notification_new("infomation-tips", notify_body, "/usr/share/infomation-tips/infomation.png");
    
    if (notify) {
        notify_notification_show(notify, NULL);
        // getchar();
    }

    return 0;
}


// gboolean        notify_init (const char *app_name);
// void            notify_uninit (void);
// gboolean        notify_is_initted (void);

// const char     *notify_get_app_name (void);
// void            notify_set_app_name (const char *app_name);

// GList          *notify_get_server_caps (void);

// gboolean        notify_get_server_info (char **ret_name,
//                                         char **ret_vendor,
//                                         char **ret_version,
//                                         char **ret_spec_version);