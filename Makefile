CALENDAR=$(shell date '+%Y%m%d')
OSID=$(shell lsb_release -si)
OSRELEASE=$(shell lsb_release -sr)
SUFFIX=
ifneq ("$(OSID)", "")
SUFFIX=_$(OSID)$(OSRELEASE)
endif

all:
	mkdir -p build
	cd build && cmake ..
	cd build && make -j12

run: all
	# exec $(shell find build/ -maxdepth 1 -type f -executable)
	exec build/infomation-tips

debug: 
	mkdir -p build
	cd build && cmake -DCMAKE_BUILD_TYPE=Debug ..
	cd build && make -j12

release:
	mkdir -p build
	cd build && cmake -DCMAKE_BUILD_TYPE=Release -DPACKAGE_SUFFIX="$(SUFFIX)" ..
	cd build && make -j12

package: release
	cd build && make package
	tree build/_CPack_Packages/Linux/DEB/infomation-tips-*
	dpkg-deb --contents build/infomation-tips_*$(CALENDAR)*$(SUFFIX).deb
	# cd build/_CPack_Packages/Linux/DEB/infomation-tips_*$(CALENDAR)*$(SUFFIX).deb && find .

copytosource:package
	cp build/infomation-tips_*$(CALENDAR)*.deb .

docker-uos:
	-rm -rf build/
	docker run --rm -it --workdir /tmp/temp -v $(shell pwd):/tmp/temp uos-professional-qt5  make package
	sudo rm -rf build/
