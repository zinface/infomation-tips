
macro(spark_install _IN_PACKAGE)
    set(CMAKE_INSTALL_PREFIX "${_IN_PACKAGE}")
endmacro(spark_install _IN_PACKAGE)

# spark_install_file
# 安装文件到指定位置
macro(spark_install_file IN_FILE IN_PATH)
    string(LENGTH ${IN_PATH} IN_PATH_LENGTH)
    if(IN_PATH_LENGTH GREATER_EQUAL 1)
        string(SUBSTRING ${IN_PATH} 0 1 IN_PATH_1)
        if(IN_PATH_1 STREQUAL "/")
            install(FILES ${IN_FILE} DESTINATION  ${IN_PATH})
        else() 
            install(FILES ${IN_FILE} DESTINATION  ${CMAKE_INSTALL_PREFIX}/${IN_PATH})
        endif(IN_PATH_1 STREQUAL "/")
    else()
        message(FATAL_ERROR "spark_install_file requires two arguments, which cannot be null")
        # string(SUBSTRING ${IN_PATH} 0 1 IN_PATH_1)
        # install(FILES ${IN_FILE} DESTINATION  ${CMAKE_INSTALL_PREFIX}/${IN_PATH})
    endif(IN_PATH_LENGTH GREATER_EQUAL 1)

endmacro(spark_install_file IN_FILE IN_PATH)

# spark_install_changelog <changelogfile>
# 安装 Changelog 文件
macro(spark_install_changelog IN_FILE)
    if(CMAKE_HOST_UNIX)
        set(_CHANGELOG_FILE "${IN_FILE}")
        if(NOT EXISTS ${_CHANGELOG_FILE})
            set(_CHANGELOG_FILE "${CMAKE_SOURCE_DIR}/${IN_FILE}")
        endif(NOT EXISTS ${_CHANGELOG_FILE})
        
        if(EXISTS ${_CHANGELOG_FILE})
            # 压缩与安装日志文件
            add_custom_command(
                OUTPUT "${CMAKE_BINARY_DIR}/changelog.gz"
                # COMMAND gzip -cn9 "${CMAKE_SOURCE_DIR}/debian/changelog" > "${CMAKE_BINARY_DIR}/changelog.gz"
                COMMAND gzip -cn9 "${_CHANGELOG_FILE}" > "${CMAKE_BINARY_DIR}/changelog.gz"
                WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}"
                COMMENT "Compressing changelog"
            )
            add_custom_target(changelog ALL DEPENDS "${CMAKE_BINARY_DIR}/changelog.gz")
            spark_install_file(${CMAKE_BINARY_DIR}/changelog.gz
                share/doc/${PROJECT_NAME}/)
        else()
            message(FATAL_ERROR "spark_install_changelog requires an absolute path.\nNot found ${IN_FILE}")
        endif(EXISTS ${_CHANGELOG_FILE})
    endif(CMAKE_HOST_UNIX)
endmacro(spark_install_changelog IN_FILE)
