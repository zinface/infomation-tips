# infomation-tips
Display system operation information on the screen

## Installation

1. Build the DEB package
2. Install the built DEB package

## Build deb

* make packae

*You get the file "\*. deb"*

## New Features

* Mouse following automatically switches to the screen where the mouse is located

## Thank you

* [lfxSpeed](https://github.com/xmuli/lfxSpeed): Read the open source code of the *`lfxSpeed`* project before development

# screenshot
![](assets/screenshot/2022-01-20-11-14-05.png)
![](assets/screenshot/2022-01-20-11-18-42.png)
![](assets/screenshot/2022-01-20-11-33-40.png)


## License